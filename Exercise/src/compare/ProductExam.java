package compare;

import java.util.Arrays;

public class ProductExam {

	public static void main(String[] args) {

		Product[] products = new Product[5];
		products[0] = new Product("A", 11);
		products[1] = new Product("Z", 13);
		products[2] = new Product("X", 10);
		products[3] = new Product("G", 15);
		products[4] = new Product("K", 20);


		System.out.println("[compareTo sort productNum]");
		Arrays.sort(products);  // compareTo() sort
		printProduct(products);

		System.out.println("[compare sort prouctName]");
		Arrays.sort(products, new ProductNameComparator());
		printProduct(products);
	}

	public static void printProduct(Product[] products) {

		for (Product product : products) {
			System.out.println(product);
		}
		//		for (int i = 0; i < product.length; i++) {
		//			System.out.println(product[i]);
		//		}
	}
}