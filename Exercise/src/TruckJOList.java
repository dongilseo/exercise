import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.stream.Stream;

public class TruckJOList {


	public static void main(String[] args) {

		//String fileName = "C:/Git/Exercise/data/Test1.csv";
		String fileName = "C://Users//Administrator//git//exercise//Exercise//data//Test1.csv";
		//		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMdd");
		//		SimpleDateFormat formatIn =  new SimpleDateFormat("ddMMMyyyy:hh:mm:ss");
		SimpleDateFormat formatOut =  new SimpleDateFormat("yyyyMMdd");
		Stream<String> stream = null;
		String[] line = null;

		Map<String, TruckVO> map = new HashMap<String, TruckVO>();

		System.out.println("Start reading JobOrder file: " + fileName);

		try {
			stream = Files.lines(Paths.get(fileName));
		} catch (IOException e) {
			e.printStackTrace();
			throw new RuntimeException("IOException while reading Truck JobOrder file, please check.");
		}

		Iterator<String> iterator = stream.iterator();
		iterator.next();// skip the 1st line
		iterator.next();// skip the 2nd line

		while (iterator.hasNext()) {
			// Example of line
			//			Area	YYYYMM	W/O Date	Vsl	BL No.	Cntr No.	Size	Type	J/O No.	Mode	Bnd	F or E	Svc Fm	Svc To	Status	Vndr Kind	Vndr Cod	Vndr Desc	J/O Input	J/O Snd	J/O User	App Date	From	From	To	To	Door	S/C	S/C Desc	Comm	Cost Code	Cost Cur	Cost	Cost Kind	Inv No
			//			0	1	2	3	4	5	6	7	8	9	10	11	12	13	14	15	16	17	18	19	20	21	22	23	24	25	26	27	28	29	30	31	32	33	34
			//			US	201701	20170114	JNTR054W	ILLB439632	DRYU9210607	4H	DC	HIIUS17D033897	R	O	F	RR	CY	S4	HSE	USBNSF05	BURLINGTON NORTHERN SANTA FE  RAILROAD	20170114	20170116	USBNLEDI	20170118	USCHI	BNL	USSPQ	YTI	USCHI	700230	GlobeRunners Incorporated	1100000000
			line = iterator.next().split(",");
			if (line[9].equals("T")) {

				TruckVO vo = new TruckVO();

				vo.setTruckJONumber(line[8]);
				vo.setTruckVendor(line[16]);

				String joNumber = vo.getTruckJONumber();

				//check unique Truck J/O
				if (!map.containsKey(joNumber)) {

					vo.setPikupLocation(line[22]);
					vo.setPikupDepot(line[23]);
					vo.setReturnLoc(line[24]);
					vo.setReturnDepot(line[25]);

					//				vo.setTruckWOSentDate(Integer.parseInt(formatOut.format(line[2])));
					//				vo.setTruckJOSentDate(Integer.parseInt(formatOut.format(line[19])));
					vo.setTruckWOSentDate(Integer.parseInt(line[2]));
					vo.setTruckJOSentDate(Integer.parseInt(line[19]));

					vo.setScNumber(line[27]);
					vo.setVesselVoyage(line[3]);
					vo.setJoborderStatus(line[14]);
					vo.setBound(line[10]);

					map.put(joNumber, vo);

				} else {
					vo = map.get(joNumber);

					EnumJobOrder jos = new EnumJobOrder();
					//need to change enum
					if (vo.getJoborderStatus().equals("S4")) {

					}
					else {

					}
				}

			}

		}

		stream.close();

		System.out.println("End reading JobOrder file: " + fileName);

	}
}