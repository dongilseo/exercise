import java.util.Map;

public class TruckVO {

	private String pikupLocation;
	private String pikupDepot;
	private String returnLoc;
	private String returnDepot;
	private String truckJONumber;
	private String truckVendor;
	private int truckWOSentDate;
	private int truckJOSentDate;
	private String scNumber;
	private String vesselVoyage;
	private String joborderStatus;
	private String bound;

	public String getPikupLocation() {
		return pikupLocation;
	}

	public void setPikupLocation(String pikupLocation) {
		this.pikupLocation = pikupLocation;
	}

	public String getPikupDepot() {
		return pikupDepot;
	}

	public void setPikupDepot(String pikupDepot) {
		this.pikupDepot = pikupDepot;
	}

	public String getReturnLoc() {
		return returnLoc;
	}

	public void setReturnLoc(String returnLoc) {
		this.returnLoc = returnLoc;
	}

	public String getReturnDepot() {
		return returnDepot;
	}

	public void setReturnDepot(String returnDepot) {
		this.returnDepot = returnDepot;
	}

	public String getTruckJONumber() {
		return truckJONumber;
	}

	public void setTruckJONumber(String truckJONumber) {
		this.truckJONumber = truckJONumber;
	}

	public String getTruckVendor() {
		return truckVendor;
	}

	public void setTruckVendor(String truckVendor) {
		this.truckVendor = truckVendor;
	}

	public int getTruckWOSentDate() {
		return truckWOSentDate;
	}

	public void setTruckWOSentDate(int truckWOSentDate) {
		this.truckWOSentDate = truckWOSentDate;
	}

	public int getTruckJOSentDate() {
		return truckJOSentDate;
	}

	public void setTruckJOSentDate(int truckJOSentDate) {
		this.truckJOSentDate = truckJOSentDate;
	}

	public String getScNumber() {
		return scNumber;
	}

	public void setScNumber(String scNumber) {
		this.scNumber = scNumber;
	}

	public String getVesselVoyage() {
		return vesselVoyage;
	}

	public void setVesselVoyage(String vesselVoyage) {
		this.vesselVoyage = vesselVoyage;
	}

	public String getJoborderStatus() {
		return joborderStatus;
	}

	public void setJoborderStatus(String joborderStatus) {
		this.joborderStatus = joborderStatus;
	}

	public String getBound() {
		return bound;
	}

	public void setBound(String bound) {
		this.bound = bound;
	}


}
