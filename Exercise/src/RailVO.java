import java.util.Map;

public class RailVO {

	private String pikupLocation;
	private String pikupDepot;
	private String returnLoc;
	private String returnDepot;
	private String railJONumber;
	private String railVendor;
	private int railWOSentDate;
	private int railJOSentDate;
	private String scNumber;
	private String vesselVoyage;
	private String joborderStatus;
	private String bound;

	public String getPikupLocation() {
		return pikupLocation;
	}

	public void setPikupLocation(String pikupLocation) {
		this.pikupLocation = pikupLocation;
	}

	public String getPikupDepot() {
		return pikupDepot;
	}

	public void setPikupDepot(String pikupDepot) {
		this.pikupDepot = pikupDepot;
	}

	public String getReturnLoc() {
		return returnLoc;
	}

	public void setReturnLoc(String returnLoc) {
		this.returnLoc = returnLoc;
	}

	public String getReturnDepot() {
		return returnDepot;
	}

	public void setReturnDepot(String returnDepot) {
		this.returnDepot = returnDepot;
	}

	public String getTruckJONumber() {
		return railJONumber;
	}

	public void setTruckJONumber(String truckJONumber) {
		this.railJONumber = truckJONumber;
	}

	public String getTruckVendor() {
		return railVendor;
	}

	public void setTruckVendor(String truckVendor) {
		this.railVendor = truckVendor;
	}

	public int getTruckWOSentDate() {
		return railWOSentDate;
	}

	public void setTruckWOSentDate(int truckWOSentDate) {
		this.railWOSentDate = truckWOSentDate;
	}

	public int getTruckJOSentDate() {
		return railJOSentDate;
	}

	public void setTruckJOSentDate(int truckJOSentDate) {
		this.railJOSentDate = truckJOSentDate;
	}

	public String getScNumber() {
		return scNumber;
	}

	public void setScNumber(String scNumber) {
		this.scNumber = scNumber;
	}

	public String getVesselVoyage() {
		return vesselVoyage;
	}

	public void setVesselVoyage(String vesselVoyage) {
		this.vesselVoyage = vesselVoyage;
	}

	public String getJoborderStatus() {
		return joborderStatus;
	}

	public void setJoborderStatus(String joborderStatus) {
		this.joborderStatus = joborderStatus;
	}

	public String getBound() {
		return bound;
	}

	public void setBound(String bound) {
		this.bound = bound;
	}


}
