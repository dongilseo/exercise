import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class TransprationDatabySQL {

	public static void main(String[] args) throws SQLException{
		// db connection
		String jdbc_driver     = "com.mysql.jdbc.Driver";
		String jdbc_url     = "jdbc:mysql://아이피:3306/board";
		String user         = "root";
		String pwd            = "패스워드";

		Connection con = null;
		Statement stmt = null;
		ResultSet rs = null;
		ResultSetMetaData metaData = null;

		try{
			// mysql 커넥션 설정
			Class.forName(jdbc_driver);
			con = DriverManager.getConnection(jdbc_url, user, pwd);
			stmt = con.createStatement();
			String sql = "select * from board";
			rs = stmt.executeQuery(sql);
			metaData = rs.getMetaData();

			// 각 행을 읽어 리스트에 저장한다.
			int sizeOfcolumn = metaData.getColumnCount();
			String column;
			List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
			Map<String, Object> map;

			while (rs.next()) {
				map = new HashMap<String,Object>();

				for(int indexOfcolumn=0; indexOfcolumn < sizeOfcolumn; indexOfcolumn++) {
					column = metaData.getColumnName(indexOfcolumn + 1);
					map.put(column, rs.getString(column));
				}
				list.add(map);
			}

			// 테스트 출력
			for (Map<String, Object> map1 : list) {
				System.out.println("=========================================");
				Iterator<String> it = map1.keySet().iterator();
				while (it.hasNext()) {
					String key = it.next();
					String value = (String)map1.get(key);

					System.out.println(key + ":::" + "\t\t\t" + value);
				}
				System.out.println("=========================================");
			}
		}catch(Exception e){
			System.out.println("데이터 베이스 연결 실패");
			System.out.println(e.getMessage());
		}finally{
			con.close();
			stmt.close();
		}
	}
}
