package collection;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

public class MapTest {

	public void putArray() {

		String valueArr[][] = {
				{"value1_1","value1_2","value1_3"},
				{"value2_1","value2_2","value2_3"},
				{"value3_1","value3_2","value3_3"}};

		Map<String, String[]> arrMap = new HashMap<>();
		arrMap.put("key01", valueArr[0]);
		arrMap.put("key02", valueArr[1]);
		arrMap.put("key03", valueArr[2]);

		LinkedList<String> valueList = new LinkedList<>();
		valueList.add("aaa");
		valueList.add("bbb");
		valueList.add("ccc");

		Map<String, LinkedList<String>> listMap = new HashMap<>();
		listMap.put("key04", valueList.get(0));
		listMap.put("key05", valueList.get(1));
		listMap.put("key06", valueList.get(2));

		Map<String, Object> multiMap = new HashMap<>();
		multiMap.put("key01", valueArr[0]);
		multiMap.put("key02", valueArr[1]);
		multiMap.put("key03", valueList.get(0));
		multiMap.put("key04", "This value type is String.");
		multiMap.put("key05", 1234);

	}
}
