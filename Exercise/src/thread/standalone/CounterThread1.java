package thread.standalone;

public class CounterThread1 extends Thread {

	Counter counter = new Counter();

	public CounterThread1(Counter counter) {
		this.counter = counter;
	}

	@Override
	public void run() {
		for(int i = 0; i < 100; i++) {
			counter.add(i);
		}
	}
}
